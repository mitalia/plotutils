#!/usr/bin/env python3
# coding: utf-8
import sys
import numpy as np
import argparse
import matplotlib as mpl
from matplotlib import pyplot as plt
from matplotlib.widgets import MultiCursor

class Series:
    def __init__(self, idx, name, scaling = 1.):
        self.idx = idx
        self.name = name
        self.scaling = scaling
        self.data = []

try:
    xrange
except:
    xrange = range

series = []

parser = argparse.ArgumentParser()
parser.add_argument('series', metavar='col_number:col_name:scaling', nargs='*', help='data series from file; if ":col_name" is omitted, the column is autonumbered; if "col_number" is omitted, the legenda read from the first line of the file is used (requires -l); if scaling is omitted, it\'s taken as 1; if col_name ends with ~ref, it is a reference series; all reference series are averaged and subtracted from all the series')
parser.add_argument('-l', '--read-legenda', action='store_true', default=False, help='Use first line as legenda')
parser.add_argument('-psh', '--pos-smooth-halfwindow', type=int, default=0, help='half window for smooth filter to apply on position data')
parser.add_argument('-vsh', '--vel-smooth-halfwindow', type=int, default=0, help='half window for smooth filter to apply on velocity data')
parser.add_argument('-ash', '--acc-smooth-halfwindow', type=int, default=0, help='half window for smooth filter to apply on acceleration data')
parser.add_argument('-jsh', '--jerk-smooth-halfwindow', type=int, default=0, help='half window for smooth filter to apply on jerk data')
parser.add_argument('-rt', '--resample-time', type=float, default=0, help='reinterpolate input data using this step')
parser.add_argument('-tc', '--time-column', type=int, default=-1, help='column to use as time axis for resampling')
parser.add_argument('-ls', '--linestyle', type=str, default='-', help='line style for the plots')
parser.add_argument('-fft', '--fft', action='store_true', default=False, help='enable FFT')
parser.add_argument('-rel', '--relative', action='store_true', default=False, help='enable relative mode; all series are offset by their last value')
parser.add_argument('-p', '--plots', type=str, default='xva', help='plots to show (x: position; v: velocity; a: acceleration; j: jerk)')
parser.add_argument('-ts', '--time-step', type=float, default=1, help='time step between two samples; used to calculate derivatives')
parser.add_argument('-s', '--style', type=str, default=None, help='matplotlib style; use LIST to display available styles')
args = parser.parse_args()
pos_smooth_window = args.pos_smooth_halfwindow*2+1
acc_smooth_window = args.acc_smooth_halfwindow*2+1
vel_smooth_window = args.vel_smooth_halfwindow*2+1
jerk_smooth_window = args.jerk_smooth_halfwindow*2+1
time_step = args.time_step
if args.style is not None:
    if args.style == 'LIST':
        print('Available styles:')
        for s in mpl.style.available:
            print('    ', s)
        sys.exit(0)
    mpl.style.use(args.style)
max_idx = -1
avail_columns = dict()
if args.read_legenda:
    for idx,name in enumerate(sys.stdin.readline().strip().split()):
        if isinstance(name, bytes):
            name = name.decode('utf-8')
        avail_columns[name] = idx

if len(args.series) == 0 and len(avail_columns) != 0:
    args.series = [':' + n for n in avail_columns.keys()]

for arg in args.series:
    argspl = arg.split(':')
    scaling = 1.
    if len(argspl) == 1:
        idx = argspl[0]
        name = 'col_' + argspl[0]
    else:
        idx,name = argspl[:2]
    if len(argspl) > 2:
        scaling = float(argspl[2])
    if idx == '':
        if name in avail_columns:
            idx = avail_columns[name]
        else:
            print("%r: index omitted but the name was not found in the legenda" % name)
            sys.exit(1)
    idx = int(idx)
    if isinstance(name, bytes):
        name = name.decode('utf-8')
    series.append(Series(idx, name, scaling))
    max_idx = max(max_idx, idx)

if args.time_column >= 0:
    series.append(Series(args.time_column, "time"))

def ravg(x, w):
    """
    Sliding window filtering of `x` with window size `w` (that must be
    odd to avoid shifting).
    """

    buf = np.full(w,x[0])
    s = x[0] * w
    p = 0
    k = 1.0 / w
    xl = len(x)
    out = np.zeros(xl + w)
    for i in xrange(xl):
        s += x[i] - buf[p]
        buf[p] = x[i]
        p = (p + 1) % w
        out[i] = s*k
    for i in xrange(w):
        s += x[-1] - buf[p]
        buf[p] = x[-1]
        p = (p + 1) % w
        out[xl+i] = s * k
    return out[w//2:-w//2]

def mk_v_a_j(data):
    speed = ravg(data[1:]-data[0:-1], vel_smooth_window)/time_step
    acc = ravg(speed[1:]-speed[0:-1], acc_smooth_window)/time_step
    jerk = ravg(acc[1:]-acc[0:-1], jerk_smooth_window)/time_step
    return (speed, acc, jerk)

def num(x: str):
    if x.startswith('0x'):
        return float(int(x, 16))
    return float(x)

for i,l in enumerate(sys.stdin):
    l = l.strip()
    if len(l)==0:
        continue
    d = tuple(num(x) for x in l.split())
    if len(series) == 0:
        for i,_ in enumerate(d):
            series.append(Series(i, "col_%d" % i))
        max_idx = len(d)-1

    if len(d) < max_idx:
        print("Not enough columns at line", i+1)
        break

    for s in series:
        s.data.append(d[s.idx] * s.scaling)

ref = None
refcount = 0

if args.time_column >= 0:
    time = np.array(series[-1].data)
    series.pop()

for s in series:
    s.data = np.array(s.data)
    if args.resample_time:
        from scipy import interpolate
        interp = interpolate.interp1d(time, s.data)
        xnew = np.arange(time[0], time[-1], args.resample_time)
        s.data = interp(xnew)

    if args.relative:
        s.data -= s.data[-1]
    if pos_smooth_window:
        s.data = ravg(s.data, pos_smooth_window)
    if s.name.endswith('~ref'):
        if ref is None:
            ref = s.data + 0
        else:
            ref += s.data
        refcount += 1

if refcount > 0:
    ref /= float(refcount)
    for s in series:
        s.data -= ref

f, axarr = plt.subplots(len(args.plots), sharex=True, squeeze=False)
axarr = [a[0] for a in axarr]
for s in series:
    v=a=j=None
    if 'v' in args.plots or 'a' in args.plots or 'j' in args.plots:
        v,a,j = mk_v_a_j(s.data)
    plots_data = {
            'x': (s.data, s.name),
            'v': (v, u"d%s/dt" % s.name),
            'a': (a, u"d²%s/dt²" % s.name),
            'j': (j, u"d³%s/dt³" % s.name)
            }
    for i,k in enumerate(args.plots):
        d,l=plots_data[k]
        axarr[i].plot(d, args.linestyle, label = l)

if args.fft:
    fig = plt.figure()
    ax = fig.add_subplot(111)
    for s in series:
        ax.plot(np.fft.fftfreq(s.data.shape[-1]), np.abs(np.fft.fft(s.data-np.mean(s.data))), args.linestyle, label='FFT(%s)'%s.name)
    axarr.append(ax)
for ax in axarr:
    ax.legend()
for fn in plt.get_fignums():
    f = plt.figure(fn)
    f.set_constrained_layout(True)

multi = MultiCursor(f.canvas, axarr, color='k', lw=.5, horizOn=False, vertOn=True)
plt.show()
