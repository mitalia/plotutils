# plotutils AKA `avx` #

`avx` is a tool born to quickly plot motion data.

It reads data from stdin (one point per row, columns separated by whitespace), plotting a chart with position (`x`), velocity (`v`) and acceleration (`a`) (configurable through `--plots` option).

By default all columns found in the file are autonumbered and plotted; specifying the `-l` option the first line is used as a legenda for column names.

You can also specify column definitions (to restrict the columns to display or to name them) from the command line, in form `column_index:label`. Without `-l`, `column_index` is mandatory (to select what column to display) but `:label` is optional; with `-l`, you can also just specify `:label` to show the column that matches that name in the legenda.

Typical usage patterns:

```bash
# read data from out.tsv, autonumber and plot all columns
avx.py < out.tsv
# same, but plot only columns 0 and 1
avx.py 0 1 < out.tsv
# same, giving a label to the columns
avx.py 0:x 1:y < out.tsv

# read data from out.tsv, using the first line as a legend, plot all columns
avx.py -l < out2.tsv
# same, but plot only columns whose name in the legenda are "x" and "y"
avx.py -l :x :y < out2.tsv
```

Velocity and acceleration are computed by crude finite increment, assuming the temporal spacing between samples is 1 (configurable through the `--time-step` option). As it can happen that input data can be not smooth enough for this, you can specify some smoothing (moving average filter) to apply at each of these passes using the `-psh`, `-vsh` and `-ash` options.

The tool has several other options, more or less random, added as I needed them. It is _not_ well written nor particularly performant - it's just a hack that I use and modify as needed.
